var 	base 	= L.tileLayer('maps/interchange/base/{z}/{x}/{y}.jpg', {
            minZoom: 13,
            maxZoom: 18
	}),
	floorm 	= L.tileLayer('maps/interchange/3floor/{z}/{x}/{y}.jpg', {
            minZoom: 13,
            maxZoom: 18
	}),
	parking = L.tileLayer('maps/interchange/parking/{z}/{x}/{y}.jpg', {
            minZoom: 13,
            maxZoom: 18
	});