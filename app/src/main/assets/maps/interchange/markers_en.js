//______________________________________________________________________________________________
var border = L.polyline([
	[0.03058, 0],
	[0.03058, 0.02821],
	[0.00000, 0.02821],
	[0, 0.00000],
	[0.03058, 0],
], 	{color: '#FFCC33'})
.addTo(mymap);

//STREET
//Dead bodys
var dead_st = L.layerGroup([]);

var locdead = [
                ["LOCATION_1",0.01061, 0.01028],
                ];

for (var i = 0; i < locdead.length; i++) {
			L.marker([locdead[i][1],locdead[i][2]], {icon: dead})
			.bindPopup("Bag")
			.addTo(dead_st);
};

//bags
var bags_st = L.layerGroup([]);

var locbags = [
                ["LOCATION_1",0.00322, 0.0122],
                ["LOCATION_2",0.0262, 0.02167],
                ["LOCATION_3",0.02151, 0.02013],
                ["LOCATION_4",0.02192, 0.01368],
                ["LOCATION_5",0.01988, 0.01459],
                ["LOCATION_6",0.01966, 0.01502],
                ["LOCATION_7",0.01775, 0.01307],
                ["LOCATION_8",0.01786, 0.01269],
                ["LOCATION_9",0.0172, 0.01485],
                ["LOCATION_10",0.01562, 0.01465],
                ["LOCATION_11",0.01526, 0.01481],
                ["LOCATION_12",0.01418, 0.01377],
                ["LOCATION_13",0.01241, 0.01284],
                ["LOCATION_14",0.01328, 0.01628],
                ["LOCATION_15",0.01914, 0.0188],
                ["LOCATION_16",0.01952, 0.01966],
                ["LOCATION_17",0.02007, 0.01926],
                ["LOCATION_18",0.01905, 0.02032],
                ["LOCATION_19",0.01536, 0.02061],
                ["LOCATION_20",0.01478, 0.02042],
                ["LOCATION_21",0.01373, 0.02013],
                ["LOCATION_22",0.00895, 0.01307],
                ["LOCATION_23",0.02011, 0.02025],

                ];

for (var i = 0; i < locbags.length; i++) {
			L.marker([locbags[i][1],locbags[i][2]], {icon: bag})
			.bindPopup("Bag")
			.addTo(bags_st);
};

//armor
var armor_st = L.layerGroup([]);

var locarm = [
                ["LOCATION_1",0.00732, 0.0123],
                ];

for (var i = 0; i < locarm.length; i++) {
			L.marker([locarm[i][1],locarm[i][2]], {icon: armor})
			.bindPopup("Armor / Clothes")
			.addTo(armor_st);
};

//loose loot
var loot_st = L.layerGroup([]);

var locloot = [
                ["LOCATION_1",0.02524, 0.0158],
		["LOCATION_2",0.02626, 0.02146],
		["LOCATION_3",0.01643, 0.00737],
		["LOCATION_4",0.01437, 0.0074],
		["LOCATION_5",0.00911, 0.01053],
		["LOCATION_6",0.0071, 0.00789],
		["LOCATION_7",0.00319, 0.01236],
		["LOCATION_7",0.00857, 0.01986],
                ];

for (var i = 0; i < locloot.length; i++) {
			L.marker([locloot[i][1],locloot[i][2]], {icon: loot})
			.bindPopup("Loose loot")
			.addTo(loot_st);
};

//PC
var pc_st = L.layerGroup([]);

var locpc = [
                ["LOCATION_1",0.01171, 0.0171],
		["LOCATION_2",0.0117, 0.01739],
		["LOCATION_3",0.01138, 0.01739],
		["LOCATION_4",0.01137, 0.01408],
		["LOCATION_5",0.01122, 0.01397],
                ];

for (var i = 0; i < locpc.length; i++) {
			L.marker([locpc[i][1],locpc[i][2]], {icon: computer})
			.bindPopup("Computer")
			.addTo(loot_st);
};



//Расставляем медицину
var meds_st = L.layerGroup([]);

var locmeds = [
                ["LOCATION_1",0.02612, 0.008],
		["LOCATION_2",0.02291, 0.01151],
		["LOCATION_3",0.01915, 0.01119],
		["LOCATION_4",0.0091, 0.00734],

                ["LOCATION_5",0.01876, 0.01454],
		["LOCATION_6",0.01866, 0.01518],
		["LOCATION_7",0.01832, 0.01454],
		["LOCATION_8",0.0172, 0.01543],
		["LOCATION_9",0.01755, 0.01454],
                ];

for (var i = 0; i < locmeds.length; i++) {
			L.marker([locmeds[i][1],locmeds[i][2]], {icon: meds})
			.bindPopup("Medicine")
			.addTo(meds_st);
};

//Расставляем оружейные ящики
var weapbox = L.layerGroup([]);

var locweapbox = [
                ["LOCATION_1",0.02904, 0.00737],
		["LOCATION_2",0.01684, 0.01136],
		["LOCATION_3",0.01591, 0.01047],
		["LOCATION_4",0.01647, 0.00782],
		["LOCATION_5",0.00641, 0.00827],
		["LOCATION_6",0.00845, 0.01971],

		["LOCATION_7",0.01663, 0.02007],
		["LOCATION_8",0.01595, 0.01813],
		["LOCATION_9",0.0125, 0.02057],
		["LOCATION_10",0.01243, 0.01998],

                ];

for (var i = 0; i < locweapbox.length; i++) {
			L.marker([locweapbox[i][1],locweapbox[i][2]], {icon: weapon_box})
			.bindPopup("Weapon box")
			.addTo(weapbox);
};




//Слои для выходов ЧВК
var 	v1 = L.marker([0.02918, 0.00246], {icon: exitpmc}),
	v1p = new L.Marker([0.02918, 0.00246], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">NORTH-WEST</div>'
    		})
	}),
    	v2 = L.marker([0.00597, 0.02557], {icon: exitpmc}),
	v2p = new L.Marker([0.00597, 0.02557], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">SOUTH-EAST</div>'
    		})
	}),
	v3  = L.marker([0.02659, 0.02307], {icon: exitmb}),
	v3p = new L.Marker([0.02659, 0.02307], { // подпись
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Electric station (3000rub.)</div>'
    		})
	});


var pmc1 = L.layerGroup([v1, v1p, v2, v2p, v3, v3p]);

//Слови для выходов Диких

var 	v1s = L.marker([0.02918, 0.00246], {icon: exitscav}),
	v1ps = new L.Marker([0.02918, 0.00246], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">NORTH-WEST</div>'
    		})
	}),
    	v2s = L.marker([0.00597, 0.02557], {icon: exitscav}),
	v2ps = new L.Marker([0.00597, 0.02557], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">SOUTH-EAST</div>'
    		})
	}),
	v3s  = L.marker([0.02659, 0.02307], {icon: exitscav}),
	v3ps = new L.Marker([0.02659, 0.02307], { // подпись
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title"> Electric station (3000rub.)</div>'
    		})
	});

var locationscavs1 = [
                ["LOCATION_1",0.01652, 0.01064],
                ["LOCATION_2",0.01605, 0.00757],
                ["LOCATION_3",0.01446, 0.00775],
		["LOCATION_4",0.02556, 0.01358],
		["LOCATION_6",0.00927, 0.0206],
                ];

var scav = L.layerGroup([v1s, v1ps, v2s, v2ps, v3s, v3ps]);

for (var i = 0; i < locationscavs1.length; i++) {
                        circles = new L.circle([locationscavs1[i][1],locationscavs1[i][2]], 80, {
				color: '#FF0000',
        			fillColor: '#FF0000',
        			fillOpacity: 0.5
			})
			.bindPopup("Possible Scavs spawn area")
			.addTo(scav);
};

//__________________________________________________________________________________________________
//PARKING
var park = L.layerGroup([]);
var locationscavs_p = [
                ["LOCATION_1",0.01976, 0.01442],
                ["LOCATION_1",0.01019, 0.02028],
                ["LOCATION_1",0.02212, 0.01712],

                ];
for (var i = 0; i < locationscavs_p.length; i++) {
                        circles = new L.circle([locationscavs_p[i][1],locationscavs_p[i][2]], 80, {
				color: '#FF0000',
        			fillColor: '#FF0000',
        			fillOpacity: 0.5
			})
			.bindPopup("Possible Scavs spawn area")
			.addTo(park);
};

//armor
var armor_st = L.layerGroup([]);

var locarm = [
                ["LOCATION_1",0.00732, 0.0123],
                ];

for (var i = 0; i < locarm.length; i++) {
			L.marker([locarm[i][1],locarm[i][2]], {icon: armor})
			.bindPopup("Armor / Clothes")
			.addTo(armor_st);
};

//main floor
//registers
var cash_st = L.layerGroup([]);

var loccash2 = [
                ["LOCATION_1",0.02424, 0.01363],
                ["LOCATION_2",0.02316, 0.0158],
                ["LOCATION_3",0.02268, 0.01583],
                ["LOCATION_4",0.02217, 0.01581],
                ["LOCATION_5",0.02034, 0.01279],
                ["LOCATION_6",0.02011, 0.01344],
                ["LOCATION_7",0.01978, 0.01358],
                ["LOCATION_8",0.01885, 0.01278],
                ["LOCATION_9",0.01762, 0.01268],
                ["LOCATION_10",0.01746, 0.01433],
                ["LOCATION_11",0.01524, 0.01374],
                ["LOCATION_12",0.014, 0.01357],
                ["LOCATION_13",0.01318, 0.01269],
                ["LOCATION_14",0.01306, 0.01367],
                ["LOCATION_15",0.01826, 0.01637],
                ["LOCATION_16",0.01988, 0.01635],
                ["LOCATION_17",0.01848, 0.01721],
                ["LOCATION_18",0.01925, 0.01816],
                ["LOCATION_19",0.01892, 0.01819],
                ["LOCATION_20",0.01857, 0.01819],
                ["LOCATION_21",0.0169, 0.0181],
                ["LOCATION_22",0.01507, 0.01817],
                ["LOCATION_23",0.01492, 0.01819],
                ["LOCATION_24",0.01469, 0.01818],
                ["LOCATION_25",0.01405, 0.0182],
                ["LOCATION_26",0.01123, 0.01495],
                ["LOCATION_27",0.01124, 0.01529],
                ["LOCATION_28",0.01125, 0.01563],
                ["LOCATION_29",0.01126, 0.01596],
                ];

for (var i = 0; i < loccash2.length; i++) {
			L.marker([loccash2[i][1],loccash2[i][2]], {icon: cash})
			.bindPopup("Loose loot")
			.addTo(loot_st);
};




//Расставляем лут
var loot_st = L.layerGroup([meds_st, loot_st, armor_st, bags_st, dead_st, cash_st, pc_st]);
//___________________________________________________________________________________________________
//FOOD COURT
//bags
var bags_food = L.layerGroup([]);

var locbags2 = [
                ["LOCATION_1",0.01935, 0.01275],
                ["LOCATION_2",0.01765, 0.01633],
                ["LOCATION_3",0.01674, 0.01703],
                ["LOCATION_4",0.01523, 0.01546],
                ["LOCATION_5",0.01248, 0.01458],
                ];

for (var i = 0; i < locbags2.length; i++) {
			L.marker([locbags2[i][1],locbags2[i][2]], {icon: bag})
			.bindPopup("Bag")
			.addTo(bags_food);
};

//loose loot
var loot_food = L.layerGroup([]);

var locloot2 = [
                ["LOCATION_1",0.02029, 0.01666],
                ["LOCATION_2",0.02011, 0.01382],
                ["LOCATION_3",0.02014, 0.0135],
                ["LOCATION_4",0.01979, 0.01358],
                ["LOCATION_5",0.01934, 0.01356],
                ["LOCATION_6",0.01863, 0.01278],
                ["LOCATION_7",0.01885, 0.01704],
                ["LOCATION_8",0.01725, 0.01395],
                ["LOCATION_9",0.0152, 0.0163],
                ["LOCATION_10",0.01324, 0.01374],
                ["LOCATION_11",0.01336, 0.01353],
                ["LOCATION_12",0.01271, 0.01275],

                ];

for (var i = 0; i < locloot2.length; i++) {
			L.marker([locloot2[i][1],locloot2[i][2]], {icon: loot})
			.bindPopup("Loose loot")
			.addTo(loot_food);
};

//registers
var cash_food = L.layerGroup([]);

var loccash = [
                ["LOCATION_1",0.01851, 0.01664],
                ["LOCATION_2",0.0181, 0.01344],
                ["LOCATION_3",0.01782, 0.01348],
                ["LOCATION_4",0.01753, 0.01345],
                ["LOCATION_5",0.01592, 0.01387],
                ["LOCATION_6",0.0157, 0.01344],
                ["LOCATION_7",0.0154, 0.01346],
                ["LOCATION_8",0.01509, 0.01346],
                ["LOCATION_9",0.01436, 0.01282],
                ["LOCATION_10",0.01334, 0.01273],
                ["LOCATION_11",0.01477, 0.01651],
                ["LOCATION_12",0.01543, 0.01655],

                ];

for (var i = 0; i < loccash.length; i++) {
			L.marker([loccash[i][1],loccash[i][2]], {icon: cash})
			.bindPopup("Registers")
			.addTo(cash_food);
};

//Расставляем медицину
var meds_food = L.layerGroup([]);

var locmeds2 = [
                ["LOCATION_1",0.01976, 0.01375],
		["LOCATION_2",0.01953, 0.01389],
		["LOCATION_3",0.01305, 0.01351],
                ];

for (var i = 0; i < locmeds2.length; i++) {
			L.marker([locmeds2[i][1],locmeds2[i][2]], {icon: meds})
			.bindPopup("Medicine")
			.addTo(meds_food);
};


//armor
var armor_food = L.layerGroup([]);

var locarm2 = [
                ["LOCATION_1",0.02053, 0.01595],
                ["LOCATION_2",0.01683, 0.0125],
                ["LOCATION_3",0.01688, 0.0169],
                ["LOCATION_4",0.01302, 0.01465],
                ];

for (var i = 0; i < locarm2.length; i++) {
			L.marker([locarm2[i][1],locarm2[i][2]], {icon: armor})
			.bindPopup("Armor / Clothes")
			.addTo(armor_food);
};

//weapon
var weapon_food = L.layerGroup([]);

var locwep2 = [
                ["LOCATION_1",0.0204, 0.01587],
                ["LOCATION_2",0.01536, 0.01542],
                ["LOCATION_3",0.01285, 0.01577],
                ];

for (var i = 0; i < locwep2.length; i++) {
			L.marker([locwep2[i][1],locwep2[i][2]], {icon: weapon})
			.bindPopup("Weapon")
			.addTo(weapon_food);
};

//Расставляем лут
var loot_food = L.layerGroup([bags_food, loot_food, cash_food, meds_food, armor_food, weapon_food]);

//___________________________________________________________________________________________________


//Слои лута
var wcrate = L.circle([51.508, -0.11], {
    color: 'white',
    fillColor: 'red',
    fillOpacity: 0,
    radius: 50
});
//Добавление слоев на карту

var baseMaps = {
    "Supermarkets (floor 1)": base,
    "Food court (floor 2)": floorm,
    "Parking lot (floor -1)": parking
};


var overlays = {
	"PMC exits": pmc1,
	"Scavs exits" : scav,
	"Scavs (floor -1)" : park,
	"W.boxes (floor 1)" : weapbox,
	"Loot (floor 1)" : loot_st,
	"Loot (floor 2)" : loot_food,
	"User's markers" : usermarkers

};


//Создание кнопки управлния слоями
L.control.layers(baseMaps, overlays).addTo(mymap);


