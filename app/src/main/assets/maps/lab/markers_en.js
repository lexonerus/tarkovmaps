//______________________________________________________________________________________________
var border = L.polyline([
	[0.0092, 0],
	[0.0092, 0.01839],
	[0.00000, 0.01839],
	[0, 0.00000],
	[0.0092, 0],
], 	{color: '#FFCC33'})
.addTo(mymap);
var linemiddle = L.polyline([
	[0.00001, 0.00913],
	[0.0092, 0.00913],
],	{color: '#FFCC33'})
.addTo(mymap);


//Слои для выходов ЧВК 
var 	v1 = L.marker([0.00773, 0.01258], {icon: exitpmc}),
	v1p = new L.Marker([0.00773, 0.01258], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">EXIT06[button in Y11 (floor 2)]</div>'
    		})
	}),
    	v2 = L.marker([0.00566, 0.01071], {icon: exitpmc}),
	v2p = new L.Marker([0.00566, 0.01071], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">EXIT01[activate in G1(floor 0)]</div>'
    		})
	}),
	v3  = L.marker([0.00418, 0.01522], {icon: exitpmc}),
	v3p = new L.Marker([0.00418, 0.01522], { // подпись
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">EXIT07[button in B24]</div>'
    		})
	});
//Активация выхода второй этаж
var 	b1 = L.marker([0.00716, 0.01401], {icon: btn}),
	b1p = new L.Marker([0.00716, 0.01401], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">EXIT06</div>'
    		})
	}),
	b2 = L.marker([0.00482, 0.01452], {icon: btn}),
	b2p = new L.Marker([0.00482, 0.01452], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">EXIT07</div>'
    		})
	});
//Активация выхода технический этаж
var 	b3 = L.marker([0.00512, 0.01353], {icon: btn}),
	b3p = new L.Marker([0.00512, 0.01353], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Fusebox for EXIT01</div>'
    		})
	}),
	b4 = L.marker([0.00449, 0.01417], {icon: btn}),
	b4p = new L.Marker([0.00449, 0.01417], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Fusebox for EXIT04</div>'
    		})
	}),
	b5 = L.marker([0.00736, 0.01605], {icon: btn}),
	b5p = new L.Marker([0.00736, 0.01605], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Fusebox for EXIT02</div>'
    		})
	}),
	b6 = L.marker([0.00426, 0.01584], {icon: btn}),
	b6p = new L.Marker([0.00426, 0.01584], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Fusebox for EXIT05</div>'
    		})
	});

//Слои для выходов ЧВК 
var 	v4 = L.marker([0.00708, 0.0169], {icon: exitpmc}),
	v4p = new L.Marker([0.00708, 0.0169], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">EXIT02</div>'
    		})
	}),
    	v5 = L.marker([0.00592, 0.0129], {icon: exitpmc}),
	v5p = new L.Marker([0.00592, 0.0129], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">EXIT03[Without Backpack]</div>'
    		})
	}),
	v6  = L.marker([0.00473, 0.01323], {icon: exitpmc}),
	v6p = new L.Marker([0.00473, 0.01323], { // подпись
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">EXIT04[Activate in G6]</div>'
    		})
	}),
	v7  = L.marker([0.00407, 0.01548], {icon: exitpmc}),
	v7p = new L.Marker([0.00407, 0.01548], { // подпись
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">EXIT05</div>'
    		})
	});

var pmc1 = L.layerGroup([v1, v1p, v2, v2p, v3, v3p]);
var pmc0 = L.layerGroup([v4, v4p, v5, v5p, v6, v6p, v7, v7p]);
var pmc2 = L.layerGroup([b1, b1p, b2, b2p]);
var pmc3 = L.layerGroup([b3, b3p, b4, b4p, b5, b5p, b6, b6p]);

var baseMaps = {
    "Technical floor (floor 0)": base,
    "First floor (floor 1)": first,
    "Second floor (floor 2)": second
};

var overlays = {
	"PMC exits (floor 0)": pmc0,
	"PMC exits (floor 1)": pmc1,
	"Exit activation points (floor 0)": pmc3,
	"Exit activation points (floor 2)": pmc2,
	"User's markers" : usermarkers

};


//Создание кнопки управлния слоями
L.control.layers(baseMaps, overlays).addTo(mymap);


