
//Необходимые ключи
var  	key1 = L.marker([0.00943, 0.0233], {icon: key}).bindPopup("Заводской ключ"),
	key2 = L.marker([0.02302, 0.02067], {icon: key}).bindPopup("Заводской ключ"),
	key3 = L.marker([0.00085, 0.00602], {icon: key}).bindPopup("Заводской ключ"),
	key4 = L.marker([0.02302, 0.02067], {icon: key}).bindPopup("Заводской ключ"),
	key5 = L.marker([0.00085, 0.00602], {icon: key}).bindPopup("Заводской ключ"),
	key6 = L.marker([0.01897, 0.01805], {icon: key}).bindPopup("Ключ от двери"),
	key7 = L.marker([0.01879, 0.01664], {icon: key}).bindPopup("Ключ от двери");

//Подписи
/*var     pump = new L.Marker([0.01871, 0.01709], {
    		icon: new L.DivIcon({
        		className: 'title',
        		html: '<div class="title">Насосная | Pump Station</div>'
    		})
	}).addTo(mymap);
*/

//Появление ключей
var  	keysp1 = L.marker([0.01483, 0.02882], {icon: keysp}).bindPopup("Место появления ключа от двери в насосную");

//Сейфы
var  	safe1 = L.marker([0.00768, 0.02235], {icon: safe}).bindPopup("Сейф");

//Расставляем точки появления оружия
var 	wp1 = L.marker([0.00918, 0.02271], {icon: weapon}).bindPopup("Оружие"),
	wp2 = L.marker([0.0081, 0.02357], {icon: weapon}).bindPopup("Оружие"),
	wp3 = L.marker([0.01331, 0.02931], {icon: weapon}).bindPopup("Оружие")

//Картотеки
var  	file1 = L.marker([0.00645, 0.02307], {icon: cabinet}).bindPopup("Картотеки");


//Расставляем лут
var loot = L.layerGroup([wp1, wp2, wp3, key1, key6, key7, keysp1, safe1, file1]);


//Инструменты
var toolboxs = [
                ["LOCATION_1",0.02311, 0.0188],
		["LOCATION_2",0.01836, 0.01747],
		["LOCATION_3",0.008, 0.00723],
		["LOCATION_4",0.01014, 0.02229]
                ];

for (var i = 0; i < toolboxs.length; i++) {
			L.marker([toolboxs[i][1],toolboxs[i][2]], {icon: tools})
			.bindPopup("Инструменты")
			.addTo(loot);
};

//Расставляем оружейные ящики
var weapbox = L.layerGroup([]);

var locweapbox = [
                ["LOCATION_1",0.01983, 0.00889],
		["LOCATION_2",0.01792, 0.01165],
		["LOCATION_3",0.0179, 0.01373],
		["LOCATION_4",0.01578, 0.0116],
		["LOCATION_5",0.01787, 0.01191],
		["LOCATION_6",0.01325, 0.00863],
		["LOCATION_7",0.01108, 0.00767],
		["LOCATION_8",0.01096, 0.00976],
		["LOCATION_9",0.01072, 0.00957],
		["LOCATION_10",0.01063, 0.00993],
		["LOCATION_11",0.01024, 0.00907],
		["LOCATION_12",0.00795, 0.01269],
		["LOCATION_13",0.00586, 0.01193],
		["LOCATION_14",0.01816, 0.02005],
                ];

for (var i = 0; i < locweapbox.length; i++) {
			L.marker([locweapbox[i][1],locweapbox[i][2]], {icon: weapon_box})
			.bindPopup("Оружейный ящик")
			.addTo(weapbox);
};



//Рисуем туннели
var tunnel1 = L.polygon([
	[0.01619, 0.01296],
	[0.01625, 0.01588],
	[0.01514, 0.01588],
	[0.01514, 0.01661],
	[0.01615, 0.01661],
	[0.01615, 0.02131],
	[0.01795, 0.02131],
	[0.01797, 0.02058],
	[0.01861, 0.02058],
	[0.01864, 0.02131],
	[0.02266, 0.02131],
	[0.02268, 0.02055],
	[0.02299, 0.02059],
	[0.023, 0.02168],
	[0.01611, 0.02174],
	[0.01563, 0.02174],
	[0.01563, 0.01945],
	[0.01473, 0.01945],
	[0.01473, 0.01548],
	[0.01549, 0.01548],
	[0.01548, 0.01296]


], 	{color: '#FF9933'})
.bindPopup("Подвалы")
.addTo(mymap);

var tunnel3 = L.polygon([
	[0.0158, 0.01201],
	[0.0158, 0.01371],
	[0.01009, 0.01371],
	[0.01009, 0.01202]

], 	{color: '#FF9933'})
.bindPopup("Подвалы")
.addTo(mymap);

var tunnel2 = L.polygon([
	[0.01258, 0.00998],
	[0.01258, 0.00914],
	[0.01155, 0.00914],
	[0.01155, 0.00939],
	[0.00959, 0.00939],
	[0.00959, 0.00818],
	[0.00738, 0.00818],
	[0.00738, 0.00939],
	[0.00404, 0.00939],
	[0.00404, 0.01002],
	[0.00608, 0.01002],
	[0.00608, 0.0115],
	[0.00873, 0.0115],
	[0.00873, 0.01083],
	[0.00663, 0.01083],
	[0.00663, 0.01],
	[0.01065, 0.01],
	[0.01065, 0.01204],
	[0.01123, 0.01204],
	[0.01123, 0.00992]


], 	{color: '#FF9933'})
.bindPopup("Подвалы")
.addTo(mymap);

//Рисуем этажи
//2-ой этаж
var floor2line = L.polygon([
	[0.01675, 0.00813],
	[0.01648, 0.0292]
], 	{color: '#99CCFF'})
.bindPopup("2-ой этаж")
.addTo(mymap);

var floor2a = L.polygon([
	[0.01785, 0.02764],
	[0.01785, 0.02991],
	[0.01266, 0.02991],
	[0.01266, 0.02764]

], 	{color: '#99CCFF'})
.bindPopup("2-ой этаж")
.addTo(mymap);

var floor2b = L.polygon([
	[0.01804, 0.00766],
	[0.01804, 0.00998],
	[0.01305, 0.00998],
	[0.01305, 0.00767]

], 	{color: '#99CCFF'})
.bindPopup("2-ой этаж")
.addTo(mymap);

//3-ий этаж
var floor3line = L.polygon([
	[0.01548, 0.00921],
	[0.00722, 0.02266]
], 	{color: '#CCFFCC'})
.bindPopup("3-ий этаж")
.addTo(mymap);

var floor3a = L.polygon([
	[0.01091, 0.02208],
	[0.01091, 0.02367],
	[0.00579, 0.02367],
	[0.00582, 0.02207]

], 	{color: '#CCFFCC'})
.bindPopup("3-ий этаж")
.addTo(mymap);

var floor3b = L.polygon([
	[0.01802, 0.00834],
	[0.01802, 0.00993],
	[0.01300, 0.00993],
	[0.01300, 0.00834]

], 	{color: '#CCFFCC'})
.bindPopup("3-ий этаж")
.addTo(mymap);

//Слои для выходов ЧВК
var 	gt3 = L.marker([0.02149, 0.00441], {icon: exitpmc}),
	gt3p = new L.Marker([0.02149, 0.00441], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Ворота 3</div>'
    		})
	}),
	podv = L.marker([0.02286, 0.02047], {icon: exitpmc}),
	podvp = new L.Marker([0.02286, 0.02047], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Подвалы</div>'
    		})
	}),
	gt0 = L.marker([0.00064, 0.0059], {icon: exitpmc}),
	gt0p = new L.Marker([0.00064, 0.0059], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Ворота 0</div>'
    		})
	});



var locationspmc = [
                ["LOCATION_1",0.0082, 0.00479],
                ["LOCATION_2",0.00446, 0.0079],
                ["LOCATION_3",0.00787, 0.00957],
                ["LOCATION_4",0.01532, 0.01708],
                ["LOCATION_5",0.01893, 0.02071],
		        ["LOCATION_6",0.02255, 0.02172]
                ];


var pmc = L.layerGroup([gt3, gt3p, podv, podvp, gt0, gt0p, key2, key3]);



for (var i = 0; i < locationspmc.length; i++) {
                        circles = new L.circle([locationspmc[i][1],locationspmc[i][2]], 100, {
				color: '#696969',
        			fillColor: '#FFFF00',
        			fillOpacity: 0.4
			})
			.bindPopup("Возможная зона появления ЧВК")
			.addTo(pmc);

};
	

//Слови для выходов Диких
var 	gt3s = L.marker([0.02149, 0.00441], {icon: exitscav}),
	gt3ps = new L.Marker([0.02149, 0.00441], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Ворота 3</div>'
    		})
	}),
	podvs = L.marker([0.02286, 0.02047], {icon: exitscav}),
	podvps = new L.Marker([0.02286, 0.02047], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Подвалы</div>'
    		})
	}),
	gt0s = L.marker([0.00064, 0.0059], {icon: exitscav}),
	gt0ps = new L.Marker([0.00064, 0.0059], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Ворота 0</div>'
    		})
	}),
	bn = L.marker([0.0085, 0.00957], {icon: exitscav}),
	bns = new L.Marker([0.0085, 0.00957], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Бункерная дверь с камерой</div>'
    		})
	}),
	ow = L.marker([0.007, 0.02226], {icon: exitscav}),
	ows = new L.Marker([0.007, 0.02226], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Офисное окно</div>'
    		})
	});


var locationscavs = [
                ["LOCATION_1",0.00889, 0.00844],
                ["LOCATION_2",0.01204, 0.01466],
                ["LOCATION_3",0.01705, 0.00995],
		["LOCATION_4",0.01842, 0.01569],
		["LOCATION_6",0.01724, 0.01923],
		["LOCATION_7",0.00814, 0.02344],
		["LOCATION_8",0.01411, 0.0288],
		["LOCATION_9",0.01642, 0.02879],
                ];

var scav = L.layerGroup([gt3s, gt3ps, podvs, podvps, gt0s, gt0ps, bn, bns, ow, ows, key4, key5]);

for (var i = 0; i < locationscavs.length; i++) {
                        circles = new L.circle([locationscavs[i][1],locationscavs[i][2]], 170, {
				color: '#FF0000',
        			fillColor: '#FF0000',
        			fillOpacity: 0.5
			})
			.bindPopup("Возможная зона появления Дикого")
			.addTo(scav);
};


//Слои лута
var wcrate = L.circle([51.508, -0.11], {
    color: 'white',
    fillColor: 'red',
    fillOpacity: 0,
    radius: 50
});

var baseMaps = {
    "Завод": base,
};

//Добавление слоев на карту
var overlays = {
	"ЧВК": pmc,
	"Дикие" : scav,
	"Оружейные ящики" : weapbox,
	"Лут" : loot,
    "Пользовательские маркеры" : usermarkers
};
//Создание кнопки управлния слоями
L.control.layers(baseMaps, overlays).addTo(mymap);
