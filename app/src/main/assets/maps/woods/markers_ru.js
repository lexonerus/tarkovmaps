//______________________________________________________________________________________________
var border = L.polyline([
	[0.04091, 0.0000],
	[0.04093, 0.0539],
	[0.00000, 0.0539],
	[0, 0.00000],
	[0.04091, 0],
], 	{color: '#FFCC33'})
.addTo(mymap);


//Расставляем оружейные ящики
var weapbox = L.layerGroup([]);

var locweapbox = [
                ["LOCATION_1",0.03546, 0.02184],
		["LOCATION_2",0.03444, 0.02284],
		["LOCATION_3",0.02492, 0.01869],
		["LOCATION_4",0.01716, 0.01318],
		["LOCATION_5",0.01684, 0.01015],
		["LOCATION_6",0.00627, 0.01885],
		["LOCATION_7",0.02078, 0.02933],
		["LOCATION_8",0.01888, 0.03078],
		["LOCATION_9",0.01383, 0.0312],
		["LOCATION_10",0.01397, 0.03221],
		["LOCATION_11",0.01283, 0.03919],
		["LOCATION_12",0.01205, 0.04474],
		["LOCATION_13",0.0119, 0.04452],
		["LOCATION_14",0.01929, 0.04875],
		["LOCATION_15",0.02172, 0.03937]
                ];

for (var i = 0; i < locweapbox.length; i++) {
			L.marker([locweapbox[i][1],locweapbox[i][2]], {icon: weapon_box})
			.bindPopup("Оружейный ящик")
			.addTo(weapbox);
};

//Расставляем точки появления оружия
var 	wp1 = L.marker([0.02203, 0.03946], {icon: weapon}).bindPopup("Оружие | Weapon");

//Мертвые тела
var 	dead1 = L.marker([0.02829, 0.03692], {icon: dead}).bindPopup("Мертвое тело");

//Стеклоочиститель
var 	spray1 = L.marker([0.02815, 0.03713], {icon: spray}).bindPopup("Стеклоочиститель");

//Палатки
var 	tent1 = L.marker([0.02094, 0.0342], {icon: tent}).bindPopup("Палатка");

//Сейфы
var  	safe1 = L.marker([0.01258, 0.02791], {icon: safe}).bindPopup("Сейф");

//Появление ключей
var  	keysp1 = L.marker([0.01282, 0.03942], {icon: keysp}).bindPopup("Место появления ключа от комнаты 314 восточного крыла.");
var  	keysp2 = L.marker([0.02621, 0.02383], {icon: keysp}).bindPopup("Место появления ключа от комнаты 316 восточного крыла санатория.");

//Пентаграмма
var  	penta1 = L.marker([0.01607, 0.03748], {icon: penta}).bindPopup("Пентаграмма. Бывает ценный лут.");

//Сумки
var  	bag1 = L.marker([0.03229, 0.01567], {icon: bag}).bindPopup("Сумка");
var  	bag2 = L.marker([0.01676, 0.03396], {icon: bag}).bindPopup("Сумка");

//Расставляем лут
var loot = L.layerGroup([wp1, dead1, spray1, tent1, safe1, keysp1, penta1, bag1, bag2, keysp2]);

//Картотеки
var files = [
                ["LOCATION_1",0.01822, 0.02704],
		["LOCATION_2",0.01282, 0.0389]
                ];

for (var i = 0; i < files.length; i++) {
			L.marker([files[i][1],files[i][2]], {icon: cabinet})
			.bindPopup("Картотеки")
			.addTo(loot);
};

//Инструменты
var toolboxs = [
                ["LOCATION_1",0.01422, 0.03129],
		["LOCATION_2",0.01286, 0.02794]
                ];

for (var i = 0; i < toolboxs.length; i++) {
			L.marker([toolboxs[i][1],toolboxs[i][2]], {icon: tools})
			.bindPopup("Инструменты")
			.addTo(loot);
};



//Слои для выходов ЧВК
var 	okr = L.marker([0.03279, 0.04441], {icon: exitpmc}),
	okrp = new L.Marker([0.03279, 0.04441], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">ОКРАИНА</div>'
    		})
	}),
    	zbo = L.marker([0.01938, 0.0484], {icon: exitmb}),
	zbop = new L.Marker([0.01938, 0.0484], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">ЗБ-014</div>'
    		})
	}),
	se  = L.marker([0.00578, 0.02632], {icon: exitmb}),
	sep = new L.Marker([0.00578, 0.02632], { // подпись
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title"> Южный А-выход (3000руб.)</div>'
    		})
	}),
	se1  = L.marker([0.00578, 0.02632], {icon: exitmb}),
	sep1 = new L.Marker([0.00578, 0.02632], { // подпись
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title"> Южный А-выход (3000руб.)</div>'
    		})
	}),
	zbt = L.marker([0.01684, 0.01025], {icon: exitmb}),
	zbtp = new L.Marker([0.01684, 0.01025], { // подпись
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">ЗБ-016</div>'
    		})
	}),
	rf  = L.marker([0.03538, 0.02207], {icon: exitmb}),
	rfp = new L.Marker([0.03538, 0.02207], { // подпись
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Ворота ВС РФ</div>'
    		})
	}),
	oon = L.marker([0.02914, 0.00353], {icon: exitpmc});
	oonp = new L.Marker([0.02914, 0.00353], { // подпись
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Блокпост ООН</div>'
    		})
	});

var locationspmc1 = [
                ["LOCATION_1",0.01026, 0.01249],
                ["LOCATION_2",0.01468, 0.00747],
                ["LOCATION_3",0.02116, 0.00524],
                ["LOCATION_4",0.02961, 0.00455],
                ["LOCATION_5",0.03399, 0.02141],
		["LOCATION_6",0.01747, 0.00549],
		["LOCATION_7",0.02545, 0.00489]
                ];

var locationspmc2 = [
                ["LOCATION_1",0.02343, 0.03631],
                ["LOCATION_2",0.02807, 0.03781],
                ["LOCATION_3",0.03077, 0.04141],
                ["LOCATION_4",0.03, 0.04648],
                ["LOCATION_5",0.02661, 0.04751],
		["LOCATION_6",0.02004, 0.04704],
                ];

var pmc1 = L.layerGroup([okr, okrp, zbo, zbop, se, sep]);
var pmc2 = L.layerGroup([se1, sep1, zbt, zbtp, rf, rfp, oon, oonp]);


for (var i = 0; i < locationspmc1.length; i++) {
                        circles = new L.circle([locationspmc1[i][1],locationspmc1[i][2]], 200, {
				color: '#696969',
        			fillColor: '#FFFF00',
        			fillOpacity: 0.4
			})
			.bindPopup("Возможная зона появления ЧВК")
			.addTo(pmc1);

};
	
for (var i = 0; i < locationspmc2.length; i++) {
                        circles = new L.circle([locationspmc2[i][1],locationspmc2[i][2]], 200, {
				color: '#696969',
        			fillColor: '#FFFF00',
        			fillOpacity: 0.4
			})
			.bindPopup("Возможная зона появления ЧВК")
			.addTo(pmc2);

};


//Слови для выходов Диких

var 	okrs = L.marker([0.03279, 0.04441], {icon: exitscav}),
	okrsp = new L.Marker([0.03279, 0.04441], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">ОКРАИНА</div>'
    		})
	}),
	dom = L.marker([0.02713, 0.04723], {icon: exitscav}),
	domp = new L.Marker([0.02713, 0.04723], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Дом диких</div>'
    		})
	}),
	okrlakes = L.marker([0.03015, 0.03847], {icon: exitscav}),
	okrlakesp = new L.Marker([0.03015, 0.03847], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">ОКРАИНА Озеро</div>'
    		})
	}),
	dead = L.marker([0.02802, 0.03672], {icon: exitscav}),
	deadp = new L.Marker([0.02802, 0.03672], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">У мертвеца</div>'
    		})
	}),
	boat = L.marker([0.02629, 0.03623], {icon: exitscav}),
	boatp = new L.Marker([0.02629, 0.03623], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Лодка</div>'
    		})
	}),
	vv = L.marker([0.01411, 0.04667], {icon: exitscav}),
	vvp = new L.Marker([0.01411, 0.04667], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Восточный выход</div>'
    		})
	}),
	mh = L.marker([0.00618, 0.0188], {icon: exitscav}),
	mhp = new L.Marker([0.00618, 0.0188], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Горный тайник</div>'
    		})
	}),
	wstb = L.marker([0.01379, 0.00546], {icon: exitscav}),
	wstbp = new L.Marker([0.01379, 0.00546], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Западная граница</div>'
    		})
	}),
	oldst = L.marker([0.02292, 0.00383], {icon: exitscav}),
	oldstp = new L.Marker([0.02292, 0.00383], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Старая станция</div>'
    		})
	}),
	oons = L.marker([0.02914, 0.00353], {icon: exitscav}),
	oonsp = new L.Marker([0.02914, 0.00353], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Блокпост ООН</div>'
    		})
	}),
	zv = L.marker([0.03331, 0.0116], {icon: exitscav}),
	zvp = new L.Marker([0.03331, 0.0116], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Ворота к заводу</div>'
    		})
	});

var locationscavs = [
                ["LOCATION_1",0.0273, 0.04728],
                ["LOCATION_2",0.02227, 0.03995],
                ["LOCATION_3",0.01279, 0.02935],
                ["LOCATION_4",0.01775, 0.02753],
                ["LOCATION_5",0.0089, 0.02002],
		["LOCATION_6",0.02444, 0.0239],
		["LOCATION_7",0.02781, 0.00914]
                ];

var scav = L.layerGroup([okrs, okrsp, dom, domp, okrlakes, okrlakesp,
			dead, deadp, boat, boatp, vv, vvp, mh, mhp,
			wstb, wstbp, oldst, oldstp, oons, oonsp, zv, zvp]);

for (var i = 0; i < locationscavs.length; i++) {
                        circles = new L.circle([locationscavs[i][1],locationscavs[i][2]], 300, {
				color: '#FF0000',
        			fillColor: '#FF0000',
        			fillOpacity: 0.5
			})
			.bindPopup("Возможная зона появления Дикого")
			.addTo(scav);
};



//Слои лута
var wcrate = L.circle([51.508, -0.11], {
    color: 'white',
    fillColor: 'red',
    fillOpacity: 0,
    radius: 50
});
//Добавление слоев на карту
var baseMaps = {
    "Заповедная зона": base,
};

var overlays = {
	"Зона появления ЧВК №1": pmc1,
	"Зона появления ЧВК №2": pmc2,
	"Дикие" : scav,
	"Оружейные ящики" : weapbox,
	"Лут" : loot,
    "Пользовательские маркеры" : usermarkers
};
//Создание кнопки управлния слоями
L.control.layers(baseMaps, overlays).addTo(mymap);
