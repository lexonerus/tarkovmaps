//_____________________________________________Дополнительные метки на карте
var border = L.polyline([
	[0.02245, 0],
	[0.02245, 0.03644],
	[0.00000, 0.03644],
	[0, 0.00000],
	[0.02245, 0],
], 	{color: '#FFCC33'})
.addTo(mymap);

//Общежитие правое крыло
var b1line = L.polyline([
	[0.01203, 0.02001],
	[0.01203, 0.02088],
	[0.01915, 0.02132],
	[0.01942, 0.01905],
], 	{color: '#99CCFF'})
.bindPopup("Правое крыло")
.addTo(mymap);

var b1floor1 = L.polygon([
	[0.01219, 0.01531],
	[0.01203, 0.02001],
	[0.00641, 0.01978],
	[0.00651, 0.0151],
], 	{color: '#99CCFF', fillOpacity: 0})
.bindPopup("Правое крыло")
.addTo(mymap);

var b1floor1b = L.polygon([
	[0.01955, 0.01849],
	[0.01947, 0.01904],
	[0.01816, 0.0189],
	[0.01821, 0.01829],
], 	{color: '#99CCFF'})
.bindPopup("Правое крыло")
.addTo(mymap);

//Общежитие левое крыло
var b2line = L.polyline([
	[0.00932, 0.0316],
	[0.01932, 0.03309],
	[0.02118, 0.01768],
	[0.0199, 0.01747],
], 	{color: '#CCFFCC'})
.bindPopup("Левое крыло")
.addTo(mymap);

var b2floor1 = L.polygon([
	[0.01051, 0.02269],
	[0.00932, 0.0316],
	[0.00304, 0.03077],
	[0.00426, 0.02183],
], 	{color: '#CCFFCC', fillOpacity: 0.1})
.bindPopup("Левое крыло")
.addTo(mymap);

var b2floor1b = L.polygon([
	[0.01998, 0.01702],
	[0.01987, 0.01791],
	[0.01932, 0.01784],
	[0.01935, 0.01748],
	[0.01862, 0.01737],
	[0.01871, 0.01683],
], 	{color: '#CCFFCC', fillOpacity: 0.6})
.bindPopup("Левое крыло")
.addTo(mymap);

//Расширение карты
var mapext = L.polygon([
	[0.01343, 0.03107],
	[0.02108, 0.03101],
	[0.02127, 0.02448],
	[0.02015, 0.01972],

	[0.01872, 0.02219],
	[0.01735, 0.02215],
	[0.01647, 0.02373],
	[0.01581, 0.02853],
	[0.01525, 0.02821],
	[0.01486, 0.02866],
	[0.01379, 0.02858],
	[0.01343, 0.03107],
], 	{
        color: '#4682B4',
        weight: 2,
        fillOpacity: 0.1
    })
.addTo(mymap);

var 	mapexttext = new L.Marker([0.01803, 0.02626], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Новый участок карты</div>'
    		})
	}).addTo(mymap);

var 	b1floor1n = new L.Marker([0.01196, 0.01645], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Этаж 1</div>'
    		})
	});
	b1floor2n = new L.Marker([0.01184, 0.0185], {
		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Этаж 2</div>'
    		})
	});
	b2floor1n = new L.Marker([0.00907, 0.02917], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Этаж 1</div>'
    		})
	});
	b2floor2n = new L.Marker([0.00944, 0.02635], {
		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Этаж 2</div>'
    		})
	});
	b2floor3n = new L.Marker([0.00972, 0.02421], {
		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Этаж 3</div>'
    		})
	});

//__________________________________________Лут в общежитиях

var rtitles = L.layerGroup([]);

//cabinets
var loccab0 = [
                ["LOCATION_1",0.00782, 0.01882],
                ["LOCATION_2",0.00716, 0.01833],

                ["LOCATION_1",0.00801, 0.02854],
                ["LOCATION_2",0.00805, 0.02995],
                ["LOCATION_1",0.00531, 0.02883],
                ];

for (var i = 0; i < loccab0.length; i++) {
			L.marker([loccab0[i][1],loccab0[i][2]], {icon: cabinet})
			.bindPopup("Картотека")
			.addTo(rtitles);
};
//loose loot
var locloot0 = [
                ["LOCATION_1",0.01117, 0.01655],
                ["LOCATION_2",0.00975, 0.01648],
                ["LOCATION_3",0.00974, 0.0172],
                ["LOCATION_4",0.00821, 0.01687],
                ["LOCATION_5",0.00818, 0.01835],

                ["LOCATION_6",0.01113, 0.019],
                ["LOCATION_7",0.00731, 0.01888],

                ["LOCATION_8",0.00744, 0.02837],
                ["LOCATION_9",0.00488, 0.0286],
                ["LOCATION_10",0.00454, 0.02851],
                ["LOCATION_11",0.00866, 0.02565],
                ["LOCATION_12",0.00854, 0.02619],
                ["LOCATION_13",0.00854, 0.02661],
                ["LOCATION_14",0.00901, 0.02456],
                ["LOCATION_15",0.00798, 0.02435],
                ];

for (var i = 0; i < locloot0.length; i++) {
			L.marker([locloot0[i][1],locloot0[i][2]], {icon: loot})
			.bindPopup("Брошенный лут")
			.addTo(rtitles);
};

//bags
var locbag0 = [
                ["LOCATION_1",0.00927, 0.01645],
                ["LOCATION_2",0.00862, 0.01654],
                ["LOCATION_3",0.0078, 0.01686],
                ["LOCATION_4",0.01032, 0.01896],
                ];

for (var i = 0; i < locbag0.length; i++) {
			L.marker([locbag0[i][1],locbag0[i][2]], {icon: bag})
			.bindPopup("Сумка")
			.addTo(rtitles);
};

//weapon boxes
var locweap0 = [
                ["LOCATION_1",0.00899, 0.01689],
                ["LOCATION_2",0.00818, 0.01637],
                ["LOCATION_3",0.0107, 0.01901],

                ["LOCATION_4",0.00529, 0.02382],
                ["LOCATION_5",0.00749, 0.02548],
                ["LOCATION_6",0.0107, 0.01901],
                ["LOCATION_7",0.00899, 0.01689],

                ];

for (var i = 0; i < locweap0.length; i++) {
			L.marker([locweap0[i][1],locweap0[i][2]], {icon: weapon_box})
			.bindPopup("Оружейный ящик")
			.addTo(rtitles);
};
//jackets
var locjac0 = [
                ["LOCATION_1",0.00795, 0.01684],
                ["LOCATION_2",0.00947, 0.01625],
                ["LOCATION_3",0.00973, 0.01843],
                ["LOCATION_4",0.01013, 0.01915],
                ["LOCATION_5",0.00751, 0.01881],
                ];

for (var i = 0; i < locjac0.length; i++) {
			L.marker([locjac0[i][1],locjac0[i][2]], {icon: jacket})
			.bindPopup("Куртка")
			.addTo(rtitles);
};

//safe
var locsaf0 = [
                ["LOCATION_1",0.01078, 0.01654],
                ["LOCATION_2",0.01088, 0.01708],
                ["LOCATION_3",0.00784, 0.0163],
                ["LOCATION_4",0.00911, 0.02549],
                ["LOCATION_5",0.00784, 0.0163],

                ];

for (var i = 0; i < locsaf0.length; i++) {
			L.marker([locsaf0[i][1],locsaf0[i][2]], {icon: safe})
			.bindPopup("Сейф")
			.addTo(rtitles);
};
//weapons
var locweap0 = [
                ["LOCATION_1",0.00501, 0.02526],
                ["LOCATION_2",0.00762, 0.02679],
                ["LOCATION_3",0.00917, 0.02468],

                ["LOCATION_4",0.00509, 0.02387],
                ];

for (var i = 0; i < locweap0.length; i++) {
			L.marker([locweap0[i][1],locweap0[i][2]], {icon: weapon})
			.bindPopup("Оружие")
			.addTo(rtitles);
};

//keys
var lockey0 = [
                ["LOCATION_1",0.01084, 0.01666],
                ["LOCATION_2",0.01082, 0.01695],
                ["LOCATION_3",0.00786, 0.01647],
                ["LOCATION_4",0.00784, 0.01676],
                ["LOCATION_5",0.00816, 0.01876],

                ["LOCATION_6",0.00792, 0.02865],
                ["LOCATION_7",0.00499, 0.02843],
                ["LOCATION_8",0.00463, 0.02837],
                ["LOCATION_9",0.00897, 0.02585],
                ["LOCATION_10",0.00502, 0.02549],

                ["LOCATION_11",0.00497, 0.02575],
                ["LOCATION_12",0.00797, 0.02681],
                ["LOCATION_13",0.00796, 0.02704],
                ["LOCATION_14",0.00848, 0.02399],
                ["LOCATION_15",0.00807, 0.02421],

                ["LOCATION_16",0.00599, 0.02353],
                ["LOCATION_17",0.00519, 0.02365],

                ];

for (var i = 0; i < lockey0.length; i++) {
			L.marker([lockey0[i][1],lockey0[i][2]], {icon: key})
			.bindPopup("Требуется ключ")
			.addTo(rtitles);
};


mymap.on('zoomend', function(e) {
	if (mymap.getZoom() > 16) {
		rtitles.addTo(mymap);
	} else {
		mymap.removeLayer(rtitles);
	}
});
//__________________________________________LOOT
var loot_st = L.layerGroup([]);
//jackets
var locjac = [
                ["LOCATION_1",0.01112, 0.00417],
                ["LOCATION_2",0.01447, 0.00787],
                ["LOCATION_3",0.00852, 0.00511],
                ["LOCATION_4",0.00945, 0.00696],
                ["LOCATION_5",0.01404, 0.01274],
                ["LOCATION_6",0.01155, 0.02276],
                ];

for (var i = 0; i < locjac.length; i++) {
			L.marker([locjac[i][1],locjac[i][2]], {icon: jacket})
			.bindPopup("Куртка")
			.addTo(loot_st);
};
//safe
var locsaf = [
                ["LOCATION_1",0.01584, 0.02392],
                ["LOCATION_2",0.01186, 0.00664],
                ];

for (var i = 0; i < locsaf.length; i++) {
			L.marker([locsaf[i][1],locsaf[i][2]], {icon: safe})
			.bindPopup("Сейф")
			.addTo(loot_st);
};

//cabinets
var loccab = [
                ["LOCATION_1",0.01374, 0.00655],
                ["LOCATION_2",0.00947, 0.00704],
                ];

for (var i = 0; i < loccab.length; i++) {
			L.marker([loccab[i][1],loccab[i][2]], {icon: cabinet})
			.bindPopup("Картотека")
			.addTo(loot_st);
};
//registers
var loccash = [
                ["LOCATION_1",0.01571, 0.01264],
                ["LOCATION_2",0.01824, 0.01714],
                ["LOCATION_3",0.0155, 0.02389],
                ["LOCATION_2",0.01558, 0.02392],
                ];

for (var i = 0; i < loccash.length; i++) {
			L.marker([loccash[i][1],loccash[i][2]], {icon: cash})
			.bindPopup("Касса")
			.addTo(loot_st);
};
//bags
var locbag = [
                ["LOCATION_1",0.01193, 0.00511],
                ["LOCATION_2",0.0082, 0.00501],
                ["LOCATION_3",0.0094, 0.00702],
                ["LOCATION_5",0.00987, 0.00466],

                ["LOCATION_6",0.01235, 0.01337],
                ["LOCATION_7",0.01544, 0.01587],
                ["LOCATION_8",0.01869, 0.02088],
                ["LOCATION_9",0.01577, 0.02378],
                ["LOCATION_10",0.01077, 0.02622],
                ];

for (var i = 0; i < locbag.length; i++) {
			L.marker([locbag[i][1],locbag[i][2]], {icon: bag})
			.bindPopup("Сумка")
			.addTo(loot_st);
};
//medicine
var locmeds = [
                ["LOCATION_1",0.01539, 0.02319],
                ["LOCATION_2",0.01574, 0.02389],
                ];

for (var i = 0; i < locmeds.length; i++) {
			L.marker([locmeds[i][1],locmeds[i][2]], {icon: meds})
			.bindPopup("Медикаменты")
			.addTo(loot_st);
};
/*//loose loot
var locloose = [
                ["LOCATION_1",0.01193, 0.00511],
                ["LOCATION_2",0.0082, 0.00501],
                ["LOCATION_3",0.0094, 0.00702],
                ["LOCATION_5",0.00987, 0.00466],

                ["LOCATION_6",0.01235, 0.01337],
                ["LOCATION_7",0.01544, 0.01587],
                ["LOCATION_8",0.01869, 0.02088],
                ["LOCATION_9",0.01577, 0.02378],
                ["LOCATION_10",0.01077, 0.02622],
                ];

for (var i = 0; i < locloose.length; i++) {
			L.marker([locloose[i][1],locloose[i][2]], {icon: loot})
			.bindPopup("Loose loot")
			.addTo(loot_st);
};*/

//Слои для выходов ЧВК
var 	zb11 = 	L.marker([0.01082, 0.03021], {icon: exitpmc}),
	zb11p = new L.Marker([0.01082, 0.03021], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">ЗБ-1011</div>'
    		})
	}),
	zb12 = 	L.marker([0.01136, 0.02545], {icon: exitmb}),
	zb12p = new L.Marker([0.01136, 0.02545], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">ЗБ-1012</div>'
    		})
	}),
	gast = 	L.marker([0.00958, 0.02108], {icon: exitmb}),
	gastp = new L.Marker([0.00958, 0.02108], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Старая заправка</div>'
    		})
	}),
	obp = 	L.marker([0.02059, 0.01825], {icon: exitmb}),
	ob = new L.Marker([0.02059, 0.01825], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Выход у общаги</div>'
    		})
	}),
	ob1p = 	L.marker([0.02059, 0.01825], {icon: exitmb}),
	ob1 = new L.Marker([0.02059, 0.01825], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Выход у общаги</div>'
    		})
	}),
	lod = 	L.marker([0.01838, 0.01107], {icon: exitmb}),
	lodp = new L.Marker([0.01838, 0.01107], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Лодка контрабандиста</div>'
    		})
	}),
	rf = 	L.marker([0.01127, 0.01182], {icon: exitmb}),
	rfp = new L.Marker([0.01127, 0.01182], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Блокпост РФ</div>'
    		})
	}),
	cros = 	L.marker([0.01232, 0.0028], {icon: exitpmc}),
	crosp = new L.Marker([0.01232, 0.0028], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Перекресток</div>'
    		})
	}),
	tr = 	L.marker([0.00829, 0.00299], {icon: exitmb}),
	trp = new L.Marker([0.00829, 0.00299], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Трейлеры</div>'
    		})
	});


var locationspmc1 = [
                ["LOCATION_1",0.00893, 0.00369],
                ["LOCATION_2",0.01219, 0.00352],
                ["LOCATION_3",0.01461, 0.00684],
                ["LOCATION_4",0.01247, 0.00551],
                ["LOCATION_5",0.00867, 0.00749],
                ["LOCATION_5",0.01133, 0.00772],
                ];

var locationspmc2 = [
                ["LOCATION_1",0.01112, 0.02774],
                ["LOCATION_2",0.01161, 0.03006],
                ["LOCATION_3",0.01296, 0.03016],
                ["LOCATION_4",0.01508, 0.02747],
                ["LOCATION_5",0.01295, 0.02857],
                ["LOCATION_6",0.01594, 0.03014],
                ["LOCATION_7",0.01746, 0.02636],
                ];

var pmc1 = L.layerGroup([zb11, zb11p, zb12, zb12p, gast, gastp, obp, ob]);
var pmc2 = L.layerGroup([ob1, ob1p, lod, lodp, rf, rfp, cros, crosp, tr, trp]);


for (var i = 0; i < locationspmc1.length; i++) {
                        circles = new L.circle([locationspmc1[i][1],locationspmc1[i][2]], 100, {
				color: '#696969',
        			fillColor: '#FFFF00',
        			fillOpacity: 0.4
			})
			.bindPopup("Возможная точка появления ЧВК")
			.addTo(pmc1);

};
	
for (var i = 0; i < locationspmc2.length; i++) {
                        circles = new L.circle([locationspmc2[i][1],locationspmc2[i][2]], 100, {
				color: '#696969',
        			fillColor: '#FFFF00',
        			fillOpacity: 0.4
			})
			.bindPopup("Возможная точка появления ЧВК")
			.addTo(pmc2);

};


//Слови для выходов Диких

var 	gasts = new L.Marker([0.00954, 0.02109], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Старая заправка</div>'
    		})
	}),
	obs = new L.Marker([0.02062, 0.01815], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Ворота у старой дороги</div>'
    		})
	}),
	ob1s = new L.Marker([0.02062, 0.01815], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Ворота у старой дороги</div>'
    		})
	}),
	lods = new L.Marker([0.01827, 0.01118], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Лодка контрабандиста</div>'
    		})
	}),
	rfs = new L.Marker([0.01127, 0.01172], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Блокпост ВС РФ</div>'
    		})
	}),
	cross = new L.Marker([0.01235, 0.00285], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Перекресток</div>'
    		})
	}),
	trs = new L.Marker([0.0082, 0.00481], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Времянка у трейлерного парка</div>'
    		})
	}),
	gd = new L.Marker([0.00906, 0.00783], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">ЖД до Таркова</div>'
    		})
	}),
	gd1 = new L.Marker([0.01606, 0.00818], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">ЖД к порту</div>'
    		})
	}),
	sk = new L.Marker([0.01251, 0.01356], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Склад 17</div>'
    		})
	}),
	sn = new L.Marker([0.01827, 0.01262], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Блокпост снайперов</div>'
    		})
	}),
	vr = new L.Marker([0.01449, 0.01951], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Заводские времянки</div>'
    		})
	}),
	sk1 = new L.Marker([0.01387, 0.02175], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Склад 4</div>'
    		})
	}),
	gd2 = new L.Marker([0.0212, 0.0262], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">ЖД к военной базе</div>'
    		})
	}),
	cc = new L.Marker([0.02069, 0.02836], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Проход между скал</div>'
    		})
	}),
	vr1 = new L.Marker([0.02099, 0.03032], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Лачуга</div>'
    		})
	}),
	kpp = new L.Marker([0.0148, 0.02866], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Военный КПП</div>'
    		})
	}),
	adm = new L.Marker([0.01322, 0.03098], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Административные ворота</div>'
    		})
	}),
	cor = new L.Marker([0.01013, 0.03058], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Дальний угол завода</div>'
    		})
	});

var locationscavs = [
                ["LOCATION_1",0.01154, 0.00427],
                ["LOCATION_2",0.01238, 0.00678],
                ["LOCATION_3",0.01489, 0.0112],
		["LOCATION_4",0.01343, 0.01277],
		["LOCATION_6",0.01616, 0.01573],
		["LOCATION_7",0.01895, 0.01792],
		["LOCATION_8",0.01438, 0.01588],
		["LOCATION_9",0.01592, 0.02236],
		["LOCATION_10",0.01199, 0.02779],
		["LOCATION_11",0.01167, 0.02916],
		["LOCATION_12",0.00985, 0.02135],
		["LOCATION_13",0.01478, 0.02783],
                ];

var scav = L.layerGroup([gasts, obs, ob1s, lods, rfs, cross, trs, gd, 
			 gd1, sk, sn, vr, sk1, gd2, cc, vr1, kpp, adm, cor]);

for (var i = 0; i < locationscavs.length; i++) {
                        circles = new L.circle([locationscavs[i][1],locationscavs[i][2]], 150, {
				color: '#FF0000',
        			fillColor: '#FF0000',
        			fillOpacity: 0.5
			})
			.bindPopup("Возможная зона появления Дикого")
			.addTo(scav);
};

//Иконки выхода Диких
var locexsc = [
                ["LOCATION_1",0.00954, 0.02109],
                ["LOCATION_2",0.02062, 0.01815],
                ["LOCATION_3",0.02062, 0.01815],
		["LOCATION_4",0.01827, 0.01118],
		["LOCATION_6",0.01127, 0.01172],
		["LOCATION_7",0.01235, 0.00285],
		["LOCATION_8",0.0082, 0.00481],
		["LOCATION_9",0.00906, 0.00783],
		["LOCATION_10",0.01606, 0.00818],
		["LOCATION_11",0.01251, 0.01356],
		["LOCATION_12",0.01827, 0.01262],
		["LOCATION_13",0.01449, 0.01951],
		["LOCATION_14",0.01387, 0.02175],
		["LOCATION_15",0.0212, 0.0262],
		["LOCATION_16",0.02069, 0.02836],
		["LOCATION_17",0.02099, 0.03032],
		["LOCATION_18",0.0148, 0.02866],
		["LOCATION_19",0.01322, 0.03098],
		["LOCATION_20",0.01013, 0.03058],
                ];

for (var i = 0; i < locexsc.length; i++) {
			L.marker([locexsc[i][1],locexsc[i][2]], {icon: exitscav})
			.addTo(scav);
};


//_____________________________________________Слои лута
//Расставляем оружейные ящики
var weapbox = L.layerGroup([]);

var locweapbox = [
                ["LOCATION_1",0.01221, 0.0027],
		["LOCATION_2",0.01218, 0.0031],
		["LOCATION_3",0.01039, 0.00624],
		["LOCATION_4",0.01246, 0.00732],
		["LOCATION_5",0.01141, 0.01146],
		["LOCATION_6",0.01543, 0.01596],

		["LOCATION_7",0.01948, 0.01568],
		["LOCATION_8",0.01513, 0.02314],
		["LOCATION_9",0.01473, 0.02477],
		["LOCATION_10",0.01021, 0.022],

		["LOCATION_11",0.01595, 0.02682],
		["LOCATION_12",0.01459, 0.02829],
		["LOCATION_13",0.01467, 0.02854],
		["LOCATION_14",0.01105, 0.02786],
		["LOCATION_15",0.01108, 0.02842],

		["LOCATION_16",0.01261, 0.0228],

                ];

for (var i = 0; i < locweapbox.length; i++) {
			L.marker([locweapbox[i][1],locweapbox[i][2]], {icon: weapon_box})
			.bindPopup("Weapon box")
			.addTo(weapbox);
};


var baseMaps = {
    "Таможня": base,
};

//Добавление слоев на карту
var overlays = {
	"Зона появления ЧВК №1": pmc1,
	"Зона появления ЧВК №2": pmc2,
	"Дикие" : scav,
	"Оружейные ящики" : weapbox,
	"Лут" : loot_st,
	"Пользовательские маркеры" : usermarkers
};
//Создание кнопки управлния слоями
L.control.layers(baseMaps, overlays).addTo(mymap);
