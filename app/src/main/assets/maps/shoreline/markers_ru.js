//______________________________________________________________________________________________
var border = L.polyline([
	[0.0158, 0.0000],
	[0.01579, 0.02486],
	[0.00000, 0.02486],
	[0, 0.00000],
	[0.0158, 0],
], 	{color: '#FFCC33'})
.addTo(mymap);


//Расставляем точки появления оружия
var 	wp1 = L.marker([0.00522, 0.00232], {icon: weapon}).bindPopup("Оружие"),
	wp2 = L.marker([0.0049, 0.00276], {icon: weapon}).bindPopup("Оружие"),
	wp3 = L.marker([0.00497, 0.00286], {icon: weapon}).bindPopup("Оружие"),
	wp4 = L.marker([0.0052, 0.00429], {icon: weapon}).bindPopup("Оружие"),
	wp5 = L.marker([0.00521, 0.00539], {icon: weapon}).bindPopup("Оружие"),
	wp6 = L.marker([0.00449, 0.00122], {icon: weapon}).bindPopup("Оружие"),
	wp7 = L.marker([0.00345, 0.00605], {icon: weapon}).bindPopup("Оружие"),
	wp8 = L.marker([0.00339, 0.00616], {icon: weapon}).bindPopup("Оружие"),
	wp9 = L.marker([0.00229, 0.00169], {icon: weapon}).bindPopup("Оружие")



//Расставляем лут
var loot = L.layerGroup([wp1, wp2, wp3, wp4, wp5, wp6, wp7, wp8, wp9]);

//Расставляем оружейные ящики
var weapbox = L.layerGroup([]);

var locweapbox = [
		//Мини-карта санатория
        ["LOCATION_1",0.00551, 0.00154],
		["LOCATION_2",0.00549, 0.00187],
		["LOCATION_3",0.00518, 0.00172],
		["LOCATION_4",0.0052, 0.00185],
		["LOCATION_5",0.00516, 0.00214],
		["LOCATION_6",0.00521, 0.00247],
		["LOCATION_7",0.0049, 0.0041],
		["LOCATION_8",0.00539, 0.00512],
		["LOCATION_9",0.00548, 0.00554],
		["LOCATION_10",0.00548, 0.0057],
		["LOCATION_11",0.00451, 0.00106],
		["LOCATION_12",0.00436, 0.00134],
		["LOCATION_13",0.00419, 0.00091],
		["LOCATION_14",0.00417, 0.00106],
		["LOCATION_15",0.00418, 0.00121],
		["LOCATION_16",0.00419, 0.00186],
		["LOCATION_17",0.00422, 0.00251],
		["LOCATION_18",0.0042, 0.00281],
		["LOCATION_19",0.00402, 0.00344],
		["LOCATION_20",0.0042, 0.00396],
		["LOCATION_21",0.00419, 0.00443],
		["LOCATION_22",0.00457, 0.00466],
		["LOCATION_23",0.0045, 0.00522],
		["LOCATION_24",0.00449, 0.00537],
		["LOCATION_25",0.0045, 0.00555],
		["LOCATION_26",0.00419, 0.00556],
		["LOCATION_27",0.00419, 0.00541],
		["LOCATION_28",0.00333, 0.00462],
		["LOCATION_29",0.00288, 0.00292],
		["LOCATION_30",0.00224, 0.00082],
		["LOCATION_31",0.00225, 0.00187],
		["LOCATION_32",0.00203, 0.00292],
		["LOCATION_33",0.00084, 0.00385],

		["LOCATION_34",0.00958, 0.00953],
		["LOCATION_35",0.01144, 0.01401],
		["LOCATION_36",0.00344, 0.00967],
		["LOCATION_37",0.0055, 0.01305],
		["LOCATION_38",0.00601, 0.01485],
		["LOCATION_39",0.00189, 0.01442],
		["LOCATION_40",0.0025, 0.01929],
		["LOCATION_41",0.00365, 0.02146],
		["LOCATION_42",0.00412, 0.02118],

		["LOCATION_43",0.00772, 0.01788],
		["LOCATION_44",0.00791, 0.01853],
		["LOCATION_45",0.00755, 0.0224],
		["LOCATION_46",0.0086, 0.022],
		["LOCATION_47",0.01035, 0.02039],
		["LOCATION_48",0.01228, 0.01954],
		["LOCATION_49",0.01226, 0.02002],
                ];

for (var i = 0; i < locweapbox.length; i++) {
			L.marker([locweapbox[i][1],locweapbox[i][2]], {icon: weapon_box})
			.bindPopup("Оружейный ящик")
			.addTo(weapbox);
};


//Слои для выходов ЧВК
var 	road = L.marker([0.00622, 0.00463], {icon: exitpmc}),
	roadp = new L.Marker([0.00622, 0.00463], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Дорога на таможню</div>'
    		})
	}),
    	lod = L.marker([0.01416, 0.01186], {icon: exitmb}),
	lodp = new L.Marker([0.01416, 0.01186], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Лодка на причале</div>'
    		})
	}),
	kpp  = L.marker([0.01283, 0.0046], {icon: exitmb}),
	kppp = new L.Marker([0.01283, 0.0046], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Военный КПП</div>'
    		})
	}),
	skal  = L.marker([0.00115, 0.0138], {icon: exitmb}),
	skalp = new L.Marker([0.00115, 0.0138], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Проход через скалы</div>'
    		})
	}),
    	lod2 = L.marker([0.01416, 0.01186], {icon: exitmb}),
	lodp2 = new L.Marker([0.01416, 0.01186], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Лодка на причале</div>'
    		})
	}),
	skal2  = L.marker([0.00115, 0.0138], {icon: exitmb}),
	skalp2 = new L.Marker([0.00115, 0.0138], { 
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Проход через скалы</div>'
    		})
	}),
	tun = L.marker([0.01051, 0.0219], {icon: exitpmc}),
	tunp = new L.Marker([0.01051, 0.0219], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Туннель</div>'
    		})
	});
var locationspmc1 = [
                ["LOCATION_1",0.01064, 0.02173],
                ["LOCATION_2",0.01007, 0.02259],
                ["LOCATION_3",0.00823, 0.02249],
                ["LOCATION_4",0.00746, 0.02222],
                ["LOCATION_5",0.00859, 0.02032],
		["LOCATION_6",0.00767, 0.01975],
		["LOCATION_7",0.00473, 0.02229],
		["LOCATION_8",0.005, 0.01968],
		["LOCATION_9",0.00293, 0.02036],
                ];

var locationspmc2 = [
                ["LOCATION_1",0.00109, 0.00946],
                ["LOCATION_2",0.00453, 0.00878],
                ["LOCATION_3",0.00597, 0.00644],
                ["LOCATION_4",0.00639, 0.00488],
                ["LOCATION_5",0.00777, 0.00694],
		["LOCATION_6",0.00902, 0.00337],
		["LOCATION_7",0.01057, 0.00568],
		["LOCATION_8",0.01112, 0.00754],
		["LOCATION_9",0.01274, 0.00541],
		["LOCATION_10",0.01312, 0.00687],
                ];

var pmc1 = L.layerGroup([road, roadp, lod, lodp, kpp, kppp, skal, skalp]);
var pmc2 = L.layerGroup([tun, tunp, lod2, lodp2, skal2, skalp2]);


for (var i = 0; i < locationspmc1.length; i++) {
                        circles = new L.circle([locationspmc1[i][1],locationspmc1[i][2]], 80, {
				color: '#696969',
        			fillColor: '#FFFF00',
        			fillOpacity: 0.4
			})
			.bindPopup("Возможная зона появления ЧВК")
			.addTo(pmc1);

};
	
for (var i = 0; i < locationspmc2.length; i++) {
                        circles = new L.circle([locationspmc2[i][1],locationspmc2[i][2]], 80, {
				color: '#696969',
        			fillColor: '#FFFF00',
        			fillOpacity: 0.4
			})
			.bindPopup("Возможная зона появления ЧВК")
			.addTo(pmc2);

};


//Слови для выходов Диких

var 	s1 = L.marker([0.0062, 0.00452], {icon: exitscav}),
	s1p = new L.Marker([0.0062, 0.00452], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Дорога на таможню</div>'
    		})
	}),
	s2 = L.marker([0.01418, 0.01019], {icon: exitscav}),
	s2p = new L.Marker([0.01418, 0.01019], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Маяк</div>'
    		})
	}),
	s3 = L.marker([0.00469, 0.01223], {icon: exitscav}),
	s3p = new L.Marker([0.00469, 0.01223], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Вход в спортзал правое крыло</div>'
    		})
	}),
	s4 = L.marker([0.0039, 0.01304], {icon: exitscav}),
	s4p = new L.Marker([0.0039, 0.01304], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Подвал в админ. корпусе</div>'
    		})
	}),
	s5 = L.marker([0.00058, 0.00944], {icon: exitscav}),
	s5p = new L.Marker([0.00058, 0.00944], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Проход в заборе на ЮГЕ</div>'
    		})
	}),
	s6 = L.marker([0.0052, 0.02323], {icon: exitscav}),
	s6p = new L.Marker([0.0052, 0.02323], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Забор у разрушенного дома</div>'
    		})
	}),
	s7 = L.marker([0.00783, 0.02291], {icon: exitscav}),
	s7p = new L.Marker([0.00783, 0.02291], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Тупик Светлый</div>'
    		})
	}),
	s8 = L.marker([0.01083, 0.02158], {icon: exitscav}),
	s8p = new L.Marker([0.01083, 0.02158], {
    		icon: new L.DivIcon({
        	className: 'title',
        	html: '<div class="title">Разрушенная дорога</div>'
    		})
	});

var locationscavs = [
                ["LOCATION_1",0.01233, 0.00621],
                ["LOCATION_2",0.00717, 0.00558],
                ["LOCATION_3",0.00987, 0.00382],
                ["LOCATION_4",0.01212, 0.01337],
                ["LOCATION_5",0.00966, 0.00968],
		["LOCATION_6",0.00875, 0.01363],
		["LOCATION_8",0.00474, 0.0135],
		["LOCATION_9",0.00215, 0.01511],
		["LOCATION_10",0.00363, 0.01863],
		["LOCATION_11",0.00783, 0.01935],
		["LOCATION_12",0.01045, 0.02126],
		["LOCATION_13",0.01217, 0.01959],
		["LOCATION_14",0.007, 0.02249],
                ];

var scav = L.layerGroup([s1,s1p,s2,s2p,s3,s3p,s4,s4p,s5,s5p,s6,s6p,s7,s7p,s8,s8p]);

for (var i = 0; i < locationscavs.length; i++) {
                        circles = new L.circle([locationscavs[i][1],locationscavs[i][2]], 120, {
				color: '#FF0000',
        			fillColor: '#FF0000',
        			fillOpacity: 0.5
			})
			.bindPopup("Возможная зона появления Дикого")
			.addTo(scav);
};



//Слои лута
var wcrate = L.circle([51.508, -0.11], {
    color: 'white',
    fillColor: 'red',
    fillOpacity: 0,
    radius: 50
});
//Добавление слоев на карту

var baseMaps = {
    "Берег": base,
};

var overlays = {
	"Зона появления ЧВК №1": pmc2,
	"Зона появления ЧВК №2": pmc1,
	"Дикие" : scav,
	"Оружейные ящики" : weapbox,
	"Лут" : loot,
    "Пользовательские маркеры" : usermarkers
};
//Создание кнопки управлния слоями
L.control.layers(baseMaps, overlays).addTo(mymap);
