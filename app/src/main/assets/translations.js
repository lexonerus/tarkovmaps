;
//Include all String translations for En an RU languages

var langid = Android.getLangId();
switch(langid) {
//********************RU
    case 0:
           var 	MAP_CLICK = "Координаты клика ",
           	    TEST_MESSAGE = "Сообщение из Android",
           	    CREATED_MARKER = "Маркер успешно создан",
           	    DELETED_MARKER = "Маркер успешно удален",
           	    LOADED_MARKER = "Пользовательские маркеры загружены",
           	    CHANGE_ICON = "Изменить иконку",
           	    DELETE_MARKER = "Удалить маркер",
           	    CREATE_MARKER = "Создать маркер",
           	    CENTER_MAP = "Центрировать карту",
           	    ZOOM_IN = "Увеличить",
           	    ZOOM_OUT = "Уменьшить",
           	    ALREADY_LOADED = "Маркеры уже загружены";

           break;
//********************EN
    case 1:
            var MAP_CLICK = "You clicked the map at ",
            	TEST_MESSAGE = "Message from Android",
            	CREATED_MARKER = "Marker was created",
            	DELETED_MARKER = "Marker was deleted",
            	LOADED_MARKER = "User-marker's were loaded",
            	CHANGE_ICON = "Change Icon",
            	DELETE_MARKER = "Delete marker",
            	CREATE_MARKER = "Create Marker",
            	CENTER_MAP = "Center map here",
            	ZOOM_IN = "Zoom in",
            	ZOOM_OUT = "Zoom out",
            	ALREADY_LOADED = "Markers are already loaded";

            break;

}
