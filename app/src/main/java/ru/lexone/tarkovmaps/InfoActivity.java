package ru.lexone.tarkovmaps;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class InfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);


//Создание кнопки Назад на панели инструментов
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        //test locale code

        //String currentLocale2 = Locale.getDefault().getLanguage();
        //TextView textLocale = (TextView)findViewById(R.id.textView);
        //textLocale.setText(currentLocale2);


    }
//Обработчик событий кнопки назад
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
