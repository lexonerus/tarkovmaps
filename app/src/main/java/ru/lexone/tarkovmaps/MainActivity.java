package ru.lexone.tarkovmaps;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (getIntent().getExtras() != null && getIntent().getExtras().getBoolean("EXIT", false)) {
            finish();
        }

        super.onCreate(savedInstanceState);
        setContentView(ru.lexone.tarkovmaps.R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(ru.lexone.tarkovmaps.R.id.toolbar);
        setSupportActionBar(toolbar);


        //Инициализируем создание базы данных
        //DatabaseHelper helper = new DatabaseHelper(this);
        //Если нет необходимости изменять базу данных используем метод getReadableDatabase()
        //SQLiteDatabase db = helper.getWritableDatabase();


        toolbar.setLogo(R.mipmap.ic_launcher);

        checkFirstRun();

        final Animation animAlpha = AnimationUtils.loadAnimation(this, R.anim.alpha);

        ImageButton imbtn1 = (ImageButton) findViewById(R.id.WoodsButton);
        imbtn1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                v.startAnimation(animAlpha);
                Intent intent = new Intent(MainActivity.this, MapActivity.class);
                String btnName = "imbtn1";
                intent.putExtra("name", btnName);
                startActivity(intent);

            }
        });

        ImageButton imbtn2 = (ImageButton) findViewById(R.id.CustomsButton);
        imbtn2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                v.startAnimation(animAlpha);
                Intent intent = new Intent(MainActivity.this, MapActivity.class);
                String btnName = "imbtn2";
                intent.putExtra("name", btnName);
                startActivity(intent);

            }
        });

        ImageButton imbtn3 = (ImageButton) findViewById(R.id.InterchangeButton);
        imbtn3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                v.startAnimation(animAlpha);
                Intent intent = new Intent(MainActivity.this, MapActivity.class);
                String btnName = "imbtn3";
                intent.putExtra("name", btnName);
                startActivity(intent);

            }
        });

        ImageButton imbtn4 = (ImageButton) findViewById(R.id.FactoryButton);
        imbtn4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                v.startAnimation(animAlpha);
                Intent intent = new Intent(MainActivity.this, MapActivity.class);
                String btnName = "imbtn4";
                intent.putExtra("name", btnName);
                startActivity(intent);

            }
        });

        ImageButton imbtn5 = (ImageButton) findViewById(R.id.ShorelineButton);
        imbtn5.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                v.startAnimation(animAlpha);
                Intent intent = new Intent(MainActivity.this, MapActivity.class);
                String btnName = "imbtn5";
                intent.putExtra("name", btnName);
                startActivity(intent);

            }
        });
        ImageButton imbtn6 = (ImageButton) findViewById(R.id.LabButton);
        imbtn6.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                v.startAnimation(animAlpha);
                Intent intent = new Intent(MainActivity.this, MapActivity.class);
                String btnName = "imbtn6";
                intent.putExtra("name", btnName);
                startActivity(intent);

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(ru.lexone.tarkovmaps.R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch(id) {
            case R.id.action_settings :
                return true;
            case R.id.action_info :
                Intent intent = new Intent(this, InfoActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_exit :
                Intent exit = new Intent(getApplicationContext(), MainActivity.class);
                exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                exit.putExtra("EXIT", true);
                startActivity(exit);
                return true;

        }

        return true;
    }

    public void checkFirstRun() {
        boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("isFirstRun", true);
        if (isFirstRun){
            // Place your dialog code here to display the dialog
            //Toast.makeText(this, "первый запуск", Toast.LENGTH_SHORT).show();
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle(R.string.important_message)
                    .setMessage(R.string.message_text)
                    .setIcon(R.drawable.update)
                    .setCancelable(false)
                    .setNegativeButton(R.string.message_button,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();

            getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                    .edit()
                    .putBoolean("isFirstRun", false)
                    .apply();
        }
    }


}
