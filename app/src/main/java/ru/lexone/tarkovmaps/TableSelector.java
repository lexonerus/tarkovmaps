package ru.lexone.tarkovmaps;

import ru.lexone.tarkovmaps.data.SqliteContract;

import static ru.lexone.tarkovmaps.MapActivity.TABLE;

public class TableSelector {
    public static String selectTable(int mapstate) {
        switch (mapstate) {
            case 1:
                TABLE = SqliteContract.StorageEntry.TABLE_NAME_1;
                break;
            case 2:
                TABLE = SqliteContract.StorageEntry.TABLE_NAME_2;
                break;
            case 3:
                TABLE = SqliteContract.StorageEntry.TABLE_NAME_3;
                break;
            case 4:
                TABLE = SqliteContract.StorageEntry.TABLE_NAME_4;
                break;
            case 5:
                TABLE = SqliteContract.StorageEntry.TABLE_NAME_5;
                break;
            case 6:
                TABLE = SqliteContract.StorageEntry.TABLE_NAME_6;
                break;
        }
        return TABLE;
    }
}
