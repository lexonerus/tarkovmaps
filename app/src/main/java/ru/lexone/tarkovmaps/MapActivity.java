package ru.lexone.tarkovmaps;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;
import ru.lexone.tarkovmaps.interfaces.WebAppInterface;
import java.util.Locale;


public class MapActivity extends AppCompatActivity {

    public static int mapstate = 0;
    public static int langid = 999;
    public static String TABLE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        String currentLocale = Locale.getDefault().getLanguage();

        Bundle extras = getIntent().getExtras();
        String catchName = extras.getString("name");

        WebView myWebView = (WebView) findViewById(R.id.webveiw);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.addJavascriptInterface(new WebAppInterface(this), "Android");

        Button button = (Button) findViewById(R.id.btn_back);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        String data = "new L.marker([0.01328, 0.01532], {icon: exitscav}).addTo(mymap);";

        if (currentLocale.equalsIgnoreCase("ru") || currentLocale.equalsIgnoreCase("be")) {
            langid = 0;
            switch (catchName) {
                case "imbtn6":
                    mapstate = 1;
                    TABLE = TableSelector.selectTable(mapstate);
                    myWebView.loadUrl("file:///android_asset/lab_ru.html");
                    break;
                case "imbtn1":
                    mapstate = 2;
                    TABLE = TableSelector.selectTable(mapstate);
                    myWebView.loadUrl("file:///android_asset/woods_ru.html");
                    break;
                case "imbtn2":
                    mapstate = 3;
                    TABLE = TableSelector.selectTable(mapstate);
                    myWebView.loadUrl("file:///android_asset/customs_ru.html");
                    break;
                case "imbtn3":
                    mapstate = 4;
                    TABLE = TableSelector.selectTable(mapstate);
                    myWebView.loadUrl("file:///android_asset/interchange_ru.html");
                    break;
                case "imbtn4":
                    mapstate = 5;
                    TABLE = TableSelector.selectTable(mapstate);
                    myWebView.loadUrl("file:///android_asset/factory_ru.html");
                    break;
                case "imbtn5":
                    mapstate = 6;
                    TABLE = TableSelector.selectTable(mapstate);
                    myWebView.loadUrl("file:///android_asset/shoreline_ru.html");
                    break;

            }
        } else {
            langid = 1;
            switch (catchName) {
                case "imbtn6":
                    mapstate = 1;
                    TABLE = TableSelector.selectTable(mapstate);
                    myWebView.loadUrl("file:///android_asset/lab_en.html");
                    break;
                case "imbtn1":
                    mapstate = 2;
                    TABLE = TableSelector.selectTable(mapstate);
                    myWebView.loadUrl("file:///android_asset/woods_en.html");
                    break;
                case "imbtn2":
                    mapstate = 3;
                    TABLE = TableSelector.selectTable(mapstate);
                    myWebView.loadUrl("file:///android_asset/customs_en.html");
                    break;
                case "imbtn3":
                    mapstate = 4;
                    TABLE = TableSelector.selectTable(mapstate);
                    myWebView.loadUrl("file:///android_asset/interchange_en.html");
                    break;
                case "imbtn4":
                    mapstate = 5;
                    TABLE = TableSelector.selectTable(mapstate);
                    myWebView.loadUrl("file:///android_asset/factory_en.html");
                    break;
                case "imbtn5":
                    mapstate = 6;
                    TABLE = TableSelector.selectTable(mapstate);
                    myWebView.loadUrl("file:///android_asset/shoreline_en.html");
                    break;
            }
        }
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }

}
