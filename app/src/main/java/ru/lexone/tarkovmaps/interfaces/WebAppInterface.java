package ru.lexone.tarkovmaps.interfaces;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.webkit.JavascriptInterface;
import android.widget.Toast;
import ru.lexone.tarkovmaps.data.DatabaseHelper;
import ru.lexone.tarkovmaps.data.SqliteContract.StorageEntry;
import static ru.lexone.tarkovmaps.MapActivity.TABLE;
import static ru.lexone.tarkovmaps.MapActivity.langid;

public class WebAppInterface {
    Context mContext;

    // Создание экземпляра интерфейса и установка контекста
    public WebAppInterface(Context c) {
        mContext = c;
    }

    // Показ всплывающего окна из веб-страницы
    @JavascriptInterface
    public  void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }
    @JavascriptInterface
    public void getString(String text) {
        String webtext = text;
        Toast.makeText(mContext, "var webtext = " + webtext, Toast.LENGTH_SHORT).show();
    }
    @JavascriptInterface
    public void insertMarker(float lat, float lng, int mid, int uniqid) {
        //Инициализируем создание базы данных
        DatabaseHelper helper = new DatabaseHelper(mContext);
        //Если нет необходимости изменять базу данных используем метод getReadableDatabase()
        SQLiteDatabase db = helper.getWritableDatabase();
        /*Для добавления или обновления нам надо создать объект ContentValues.
        Данный объект представляет словарь, который содержит набор пар
        "ключ-значение". Для добавления в этот словарь нового объекта применяется
        метод put. Первый параметр метода - это ключ, а второй - значение.*/
        ContentValues values = new ContentValues();
        values.put(StorageEntry.COLUMN_ID, mid);
        values.put(StorageEntry.COLUMN_UNIQID, uniqid);
        values.put(StorageEntry.COLUMN_LAT, lat);
        values.put(StorageEntry.COLUMN_LNG, lng);
        long storage_id = db.insert(TABLE, null, values);
        db.close();
    }

    @JavascriptInterface
    public void deleteMarker(int uniqid) {
        // подключаемся к БД
        DatabaseHelper helper = new DatabaseHelper(mContext);
        SQLiteDatabase db = helper.getWritableDatabase();
        int delCount = db.delete(TABLE, "uniqid = " + uniqid, null);
        // закрываем БД
        db.close();
    }

    @JavascriptInterface
    public void setIcon(int id, int a) {
        DatabaseHelper helper = new DatabaseHelper(mContext);
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put("icon", a);
        db.update(TABLE, newValues, "uniqid = " + id, null);
        // закрываем БД
        db.close();
    }

    @JavascriptInterface
    public void updateLatLng(float lat, float lng, int uniqid) {
        DatabaseHelper helper = new DatabaseHelper(mContext);
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put("lat", lat);
        newValues.put("lng", lng);
        db.update(TABLE, newValues, "uniqid = " + uniqid, null);
        // закрываем БД
        db.close();
    }

    @JavascriptInterface
    public int restoreIcon(int uniqid) {
        DatabaseHelper helper = new DatabaseHelper(mContext);
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT icon FROM " + TABLE + " WHERE uniqid = " + uniqid, null);
        cursor.moveToFirst();
        int icon = cursor.getInt(0);
        cursor.close();
        db.close();
        return icon;
    }

    @JavascriptInterface
    public float getLat(int j) {
        DatabaseHelper helper = new DatabaseHelper(mContext);
        SQLiteDatabase db = helper.getReadableDatabase();
        String[] projection = {StorageEntry.COLUMN_LAT};
        Cursor cursor = db.query(TABLE, projection, null, null, null, null, null);
        cursor.moveToPosition(j);
        float lat = cursor.getFloat(0);
        cursor.close();
        db.close();
        return lat;
    }
    @JavascriptInterface
    public float getLng(int j) {
        DatabaseHelper helper = new DatabaseHelper(mContext);
        SQLiteDatabase db = helper.getReadableDatabase();
        String[] projection = {StorageEntry.COLUMN_LNG};
        Cursor cursor = db.query(TABLE, projection, null, null, null, null, null);
        cursor.moveToPosition(j);
        float lng = cursor.getFloat(0);
        cursor.close();
        db.close();
        return lng;
    }

    @JavascriptInterface
    public int getUniqid(int j) {
        DatabaseHelper helper = new DatabaseHelper(mContext);
        SQLiteDatabase db = helper.getReadableDatabase();
        String[] projection = {StorageEntry.COLUMN_UNIQID};
        Cursor cursor = db.query(TABLE, projection, null, null, null, null, null);
        cursor.moveToPosition(j);
        int uniqid = cursor.getInt(0);
        cursor.close();
        db.close();
        return uniqid;
    }

    @JavascriptInterface
    public int getUniqidLast() {
        int uniqid = 0;
        DatabaseHelper helper = new DatabaseHelper(mContext);
        SQLiteDatabase db = helper.getReadableDatabase();
        String[] projection = {StorageEntry.COLUMN_UNIQID};
        Cursor cursor = db.query(TABLE, projection, null, null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToLast();
            uniqid = cursor.getInt(0);
            cursor.close();
            db.close();
            return uniqid+1;
        } else {
            cursor.close();
            db.close();
            return 0;
        }
    }

    @JavascriptInterface
    public int getUniqidByMarkid(int lat, int lng) {
        int uniqid = 0;
        DatabaseHelper helper = new DatabaseHelper(mContext);
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT uniqid FROM " + TABLE + " WHERE lat = " + lat + " AND lng = " + lng, null);
        cursor.moveToFirst();
        int result = cursor.getInt(0);
        cursor.close();
        db.close();
        return result;
    }

    @JavascriptInterface
    public int getDataLength() {
        DatabaseHelper helper = new DatabaseHelper(mContext);
        SQLiteDatabase db = helper.getReadableDatabase();
        String[] projection = {StorageEntry.COLUMN_ID};
        Cursor cursor = db.query(TABLE, projection, null, null, null, null, null);
        Integer[] array = new Integer[cursor.getCount()];
        int i = 0;
        while(cursor.moveToNext()){
            int uname = cursor.getColumnIndex("id");
            array[i] = uname;
            i++;
        }
        cursor.close();
        db.close();
        return i;
    }

    @JavascriptInterface
    public String modifyString(String inputString) {
        return inputString + " from Java side.";
    }

    @JavascriptInterface
    public int getLangId() {

        return langid;
    }



}
