package ru.lexone.tarkovmaps.data;

import android.provider.BaseColumns;

public final class SqliteContract {
    public static final class StorageEntry implements BaseColumns {
        // Имена таблиц
        public static final String TABLE_NAME_1 = "lab";
        public static final String TABLE_NAME_2 = "woods";
        public static final String TABLE_NAME_3 = "customs";
        public static final String TABLE_NAME_4 = "interchange";
        public static final String TABLE_NAME_5 = "factory";
        public static final String TABLE_NAME_6 = "shoreline";

        // Имена столбцов
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_UNIQID = "uniqid";
        public static final String COLUMN_LAT = "lat";
        public static final String COLUMN_LNG = "lng";
        public static final String COLUMN_ICON = "icon";
        public static final String COLUMN_ATTR = "attr";
        public static final String COLUMN_POP = "poppup";
        public static final String COLUMN_FEATURE = "feature";
    }
}
