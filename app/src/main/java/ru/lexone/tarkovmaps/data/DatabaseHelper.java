package ru.lexone.tarkovmaps.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import ru.lexone.tarkovmaps.data.SqliteContract.StorageEntry;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "markers_storage.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_1_CREATE =
            "CREATE TABLE " + StorageEntry.TABLE_NAME_1 + " (" +
                    StorageEntry._ID + " INTEGER PRIMARY KEY, " +
                    StorageEntry.COLUMN_ID + " INTEGER, " +
                    StorageEntry.COLUMN_UNIQID + " INTEGER UNIQUE, " +
                    StorageEntry.COLUMN_LAT + " REAL, " +
                    StorageEntry.COLUMN_LNG + " REAL, " +
                    StorageEntry.COLUMN_ICON + " INTEGER DEFAULT 0," +
                    StorageEntry.COLUMN_POP + " TEXT DEFAULT 'no pop'," +
                    StorageEntry.COLUMN_ATTR + " TEXT DEFAULT 'no attr'," +
                    StorageEntry.COLUMN_FEATURE + " TEXT DEFAULT 'no feature'" +
                    ")";
    private static final String TABLE_2_CREATE =
            "CREATE TABLE " + StorageEntry.TABLE_NAME_2 + " (" +
                    StorageEntry._ID + " INTEGER PRIMARY KEY, " +
                    StorageEntry.COLUMN_ID + " INTEGER, " +
                    StorageEntry.COLUMN_UNIQID + " INTEGER UNIQUE, " +
                    StorageEntry.COLUMN_LAT + " REAL, " +
                    StorageEntry.COLUMN_LNG + " REAL, " +
                    StorageEntry.COLUMN_ICON + " INTEGER DEFAULT 0," +
                    StorageEntry.COLUMN_POP + " TEXT DEFAULT 'no pop'," +
                    StorageEntry.COLUMN_ATTR + " TEXT DEFAULT 'no attr'," +
                    StorageEntry.COLUMN_FEATURE + " TEXT DEFAULT 'no feature'" +
                    ")";
    private static final String TABLE_3_CREATE =
            "CREATE TABLE " + StorageEntry.TABLE_NAME_3 + " (" +
                    StorageEntry._ID + " INTEGER PRIMARY KEY, " +
                    StorageEntry.COLUMN_ID + " INTEGER, " +
                    StorageEntry.COLUMN_UNIQID + " INTEGER UNIQUE, " +
                    StorageEntry.COLUMN_LAT + " REAL, " +
                    StorageEntry.COLUMN_LNG + " REAL, " +
                    StorageEntry.COLUMN_ICON + " INTEGER DEFAULT 0," +
                    StorageEntry.COLUMN_POP + " TEXT DEFAULT 'no pop'," +
                    StorageEntry.COLUMN_ATTR + " TEXT DEFAULT 'no attr'," +
                    StorageEntry.COLUMN_FEATURE + " TEXT DEFAULT 'no feature'" +
                    ")";
    private static final String TABLE_4_CREATE =
            "CREATE TABLE " + StorageEntry.TABLE_NAME_4 + " (" +
                    StorageEntry._ID + " INTEGER PRIMARY KEY, " +
                    StorageEntry.COLUMN_ID + " INTEGER, " +
                    StorageEntry.COLUMN_UNIQID + " INTEGER UNIQUE, " +
                    StorageEntry.COLUMN_LAT + " REAL, " +
                    StorageEntry.COLUMN_LNG + " REAL, " +
                    StorageEntry.COLUMN_ICON + " INTEGER DEFAULT 0," +
                    StorageEntry.COLUMN_POP + " TEXT DEFAULT 'no pop'," +
                    StorageEntry.COLUMN_ATTR + " TEXT DEFAULT 'no attr'," +
                    StorageEntry.COLUMN_FEATURE + " TEXT DEFAULT 'no feature'" +
                    ")";
    private static final String TABLE_5_CREATE =
            "CREATE TABLE " + StorageEntry.TABLE_NAME_5 + " (" +
                    StorageEntry._ID + " INTEGER PRIMARY KEY, " +
                    StorageEntry.COLUMN_ID + " INTEGER, " +
                    StorageEntry.COLUMN_UNIQID + " INTEGER UNIQUE, " +
                    StorageEntry.COLUMN_LAT + " REAL, " +
                    StorageEntry.COLUMN_LNG + " REAL, " +
                    StorageEntry.COLUMN_ICON + " INTEGER DEFAULT 0," +
                    StorageEntry.COLUMN_POP + " TEXT DEFAULT 'no pop'," +
                    StorageEntry.COLUMN_ATTR + " TEXT DEFAULT 'no attr'," +
                    StorageEntry.COLUMN_FEATURE + " TEXT DEFAULT 'no feature'" +
                    ")";
    private static final String TABLE_6_CREATE =
            "CREATE TABLE " + StorageEntry.TABLE_NAME_6 + " (" +
                    StorageEntry._ID + " INTEGER PRIMARY KEY, " +
                    StorageEntry.COLUMN_ID + " INTEGER, " +
                    StorageEntry.COLUMN_UNIQID + " INTEGER UNIQUE, " +
                    StorageEntry.COLUMN_LAT + " REAL, " +
                    StorageEntry.COLUMN_LNG + " REAL, " +
                    StorageEntry.COLUMN_ICON + " INTEGER DEFAULT 0," +
                    StorageEntry.COLUMN_POP + " TEXT DEFAULT 'no pop'," +
                    StorageEntry.COLUMN_ATTR + " TEXT DEFAULT 'no attr'," +
                    StorageEntry.COLUMN_FEATURE + " TEXT DEFAULT 'no feature'" +
                    ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(TABLE_1_CREATE);
        db.execSQL(TABLE_2_CREATE);
        db.execSQL(TABLE_3_CREATE);
        db.execSQL(TABLE_4_CREATE);
        db.execSQL(TABLE_5_CREATE);
        db.execSQL(TABLE_6_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + StorageEntry.TABLE_NAME_1);
        db.execSQL("DROP TABLE IF EXISTS " + StorageEntry.TABLE_NAME_2);
        db.execSQL("DROP TABLE IF EXISTS " + StorageEntry.TABLE_NAME_3);
        db.execSQL("DROP TABLE IF EXISTS " + StorageEntry.TABLE_NAME_4);
        db.execSQL("DROP TABLE IF EXISTS " + StorageEntry.TABLE_NAME_5);
        db.execSQL("DROP TABLE IF EXISTS " + StorageEntry.TABLE_NAME_6);
        onCreate(db);
    }
}
